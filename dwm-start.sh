# dwm-start.sh - A script to start dwm and other default programs.
#!/bin/bash

# battery name (replace with your own)
BATTNAME="cw2015-battery"
BATTPATH="/sys/class/power_supply/${BATTNAME}"

# startup applications
xcompmgr -c &  # compositor
feh --bg-scale ~/Pictures/Wallpapers/bg.png &  # wallpaper
udiskie & # automount external drives

# set status bar info
while true; do
	# get battery info
	BATT="$(cat ${BATTPATH}/capacity)"
	STATUS="$(cat ${BATTPATH}/status)"
	# display battery and time
	xsetroot -name "`echo Battery: $BATT $STATUS` | `date "+%Y-%m-%d ( '3')~ %R"`"
	sleep 1
done &

# start dwm
exec dwm

