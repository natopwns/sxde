# sxde

My configuration of `dwm` and friends. I am attempting to make it as user-friendly as possible, so that non-linux geeks can use it effectively.

## Installing

Install the prerequisite programs (script coming soon). Open `dwm-start.sh` and fill in your battery folder name in the `BATT` variable at the beginning. Then, run `sudo ./install-sxde.sh`.

You will now have a `dwm` entry in your display manager (if you have one). It you don't have a display manager, you can copy the contents of `dwm-start.sh` to a `.xinitrc` and run `startx`.

## Usage

Right now, it's just dwm, so go to suckless.org to read up on that. Eventually, I'd like for it to be "user-friendly" dwm. Still lightweight, but with some helpful text to explain things and clicky buttons on a status bar.

## Contributing

I'm pretty new to collaborating on GitLab and all that, but feel free to help out. I'll try to keep learning how that stuff works.
