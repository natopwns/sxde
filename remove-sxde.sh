#!/bin/bash

# remove desktop entry and startup script
sudo rm /usr/share/xsessions/dwm.desktop
sudo rm /usr/local/bin/dwm-start.sh

# remove configs
sudo rm /etc/X11/xorg.conf.d/30-touchpad.conf

# remove scripts
sudo rm /usr/local/bin/brightness-down.sh
sudo rm /usr/local/bin/brightness-up.sh

# remove sudoers entries
sudo rm /etc/sudoers.d/xbacklight.conf
