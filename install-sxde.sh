#!/bin/bash

# copy desktop entry and startup script
sudo cp dwm.desktop /usr/share/xsessions/
sudo cp dwm-start.sh /usr/local/bin/
sudo chmod +x /usr/local/bin/dwm-start.sh  # mark startup script as executable

# copy configs over
sudo cp Configs/30-touchpad.conf /etc/X11/xorg.conf.d/

# copy scripts over
sudo cp Scripts/brightness-down.sh /usr/local/bin/
sudo chmod +x /usr/local/bin/brightness-down.sh
sudo cp Scripts/brightness-up.sh /usr/local/bin/
sudo chmod +x /usr/local/bin/brightness-up.sh

# allow xbacklight (acpilight) to work without a password
echo "$USER ALL=(ALL) NOPASSWD: /usr/bin/xbacklight" | sudo EDITOR='tee' visudo --file=/etc/sudoers.d/01_xbacklight

